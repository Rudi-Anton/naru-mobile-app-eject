import React from "react";
import { StatusBar, Image, MapView, Alert, AsyncStorage, WebView, View, TouchableOpacity, StyleSheet, Linking, TextInput } from "react-native";
import Expo from "expo";
import {
  Container,
  Header,
  Content,
  Form,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Label,
  ListItem,
  CheckBox,
  Thumbnail,
  Text, Avatar
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Modal from 'react-native-modal';

export default class Donasi extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      idNews: "",
      isReady: true,
      nama: "",
      email: "",
      nominal: "",
      phonenumber: "",
      payment: "",
      warnacard: 'white',
      isModalVisible: false,
      warnamoney: 'white'
    }
  }


  _showModalT = () => {
    this.state.payment = 'T';
    if (this.state.nama == null || this.state.nama == "" || this.state.email == null || this.state.email == "" ||
      this.state.nominal == null || this.state.nominal == "" || this.state.payment == null || this.state.payment == "" ||
      this.state.phonenumber == null || this.state.phonenumber == "") {
      Alert.alert(
        'Pesan',
        'Nama / Email / Nominal / Nomer Telpon tidak boleh kosong',
        [
          { text: 'OK', style: 'cancel' },
        ],
        { cancelable: false }
      )
    }
    else {
      this.setState({ isModalVisible: true })
    }

  }

  render() {

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Donasi" />
        <Content>
          <Card>
            <CardItem>
              <Text>Tulis Jumlah Donasi Anda</Text>
            </CardItem>
            <CardItem>
              <Item regular>
                <Input keyboardType='numeric' onChangeText={this.handleNominal} placeholder="Rp. 0" resizeMode='contain' />
              </Item>
            </CardItem>
            <CardItem>
              <Text>Harap Memasukan Identitas Anda</Text>
            </CardItem>
            <CardItem style={{ height: 50, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity disabled>
                <Image style={{ width: 150 }} source={require("../../img/asset/donasi/facebook.png")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity disabled>
                <Image style={{ width: 150 }} source={require("../../img/asset/donasi/google.png")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
            <CardItem>
              <Text>Atau</Text>
            </CardItem>
            <CardItem style={{ height: 60, flex: 1, justifyContent: 'space-between', flexDirection: "row" }}>
              <TextInput
                underlineColorAndroid='white'
                style={{
                  textAlign: "center",
                  width: '45%',
                  height: 50,
                  borderColor: 'gray',
                  borderWidth: 0.4,
                }} onChangeText={this.handleNama} placeholder="Nama Lengkap" />
              <TextInput
                underlineColorAndroid='white'
                style={{
                  textAlign: "center",
                  width: '45%',
                  height: 50,
                  borderColor: 'gray',
                  borderWidth: 0.4,
                }} onChangeText={this.handleEmail} placeholder="Alamat Email" />
            </CardItem>
            <CardItem>
              <Text>Dan nomor ponsel anda untuk menerima SMS status donasi :</Text>
            </CardItem>
            <CardItem>
              <Item regular>
                <Input keyboardType='numeric' onChangeText={this.handlePhoneNumber} placeholder="081xxx atau 62xxx (angka saja)" />
              </Item>
            </CardItem>
            <CardItem>
              <Text>Pilih alat pembayaran :</Text>
            </CardItem>
            <CardItem style={{ flex: 1, justifyContent: 'space-between', flexDirection: "row" }}>
              <Button onPress={this._showModalT} value='T-money' style={{ backgroundColor: 'white' }}>
                <Image style={{ width: 150 }} source={require("../../img/asset/donasi/t-money.png")} resizeMode='contain' />
              </Button>
              <Button onPress={this.posesDonasiCC} value='CC' style={{ backgroundColor: 'white' }}>
                <Image style={{ width: 150 }} source={require("../../img/asset/donasi/cc-card.png")} resizeMode='contain' />
              </Button>
            </CardItem>
          </Card>
        </Content>
        <Modal isVisible={this.state.isModalVisible} style={{ width: '90%', height: 1000 }}>
          <Card>
            <CardItem style={{ justifyContent: 'space-between', flexDirection: "row" }}>
              <Text style={{ fontSize: 15, fontWeight: 'bold' }} >Rincian Donasi</Text>
              <TouchableOpacity onPress={this._hideModal}>
                <Image style={{ height: 30, width: 30 }} source={require("../../img/asset/ic_close.png")} />
              </TouchableOpacity>
            </CardItem>
            <CardItem style={{ justifyContent: 'space-between', flexDirection: "row" }}>
              <Text >Nama</Text>
              <Text >{this.state.nama}</Text>
            </CardItem>
            <CardItem style={{ justifyContent: 'space-between', flexDirection: "row" }}>
              <Text >No. HP</Text>
              <Text >{this.state.phonenumber}</Text>
            </CardItem>
            <CardItem style={{ justifyContent: 'space-between', flexDirection: "row" }}>
              <Text >Jumlah</Text>
              <Text >{this.state.nominal}</Text>
            </CardItem>
            <CardItem >
              <Text style={{ fontSize: 15, fontWeight: 'bold' }} >Masukkan akun T-Money anda</Text>
            </CardItem>
            <CardItem >
              <TextInput
                underlineColorAndroid='white'
                style={{
                  textAlign: "center",
                  width: '100%',
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 0.4,
                }} onChangeText={this.handleAkun} placeholder="Akun T-Money" resizeMode='contain' />
            </CardItem>
            <CardItem >
              <TextInput
                secureTextEntry={true}
                underlineColorAndroid='white'
                style={{
                  textAlign: "center",
                  width: '100%',
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 0.4,
                }} onChangeText={this.handlePassword} placeholder="Password" resizeMode='contain' />
            </CardItem>
            <CardItem >
              <TextInput
                secureTextEntry={true}
                underlineColorAndroid='white'
                style={{
                  textAlign: "center",
                  width: '100%',
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 0.4,
                }} onChangeText={this.handlePin} placeholder="Pin" resizeMode='contain' />
            </CardItem>
            <CardItem style={{ justifyContent: 'space-between', flexDirection: "row" }}>
              <Button onPress={this.posesDonasi} style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%", width: responsiveWidth(70) }}><Text style={{ textAlign: "center" }}> Transfer Sekarang </Text></Button>
            </CardItem>
          </Card>
        </Modal>
      </Container>
    );
  }

  handleNama = (text) => {
    this.setState({ nama: text })
    // console.log(this.state.name)
  }
  handleEmail = (text) => {
    this.setState({ email: text })
  }
  handleNominal = (text) => {
    this.setState({ nominal: text })
  }
  handlePhoneNumber = (text) => {
    this.setState({ phonenumber: text })
  }
  handlePayment = (text) => {
    this.setState({ payment: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  handleAkun = (text) => {
    this.setState({ akun: text })
  }
  handlePin = (text) => {
    this.setState({ pin: text })
  }

  _hideModal = () => {


    this.setState({ isModalVisible: false })
  }

  posesDonasiCC = () => {
    this.state.payment = 'CC'
    var param = {
      store: "19509",
      auth_key: "nkDCS-WLZg^XkzTB",
      amount: this.state.nominal,
      name: this.state.nama,
      idNews: "10"
    };
    var formBody = [];
    for (var property in param) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(param[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    //Fin Pay CC
    return fetch("http://ec2-54-255-179-65.ap-southeast-1.compute.amazonaws.com:9000/api/finpaycc/donation-mainapi", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
      body: formBody
    })
      .then(response => response.json())
      .then((data) => {

        AsyncStorage.setItem("link", data.data.order.url);
        AsyncStorage.getItem("link", (error, result) => {
          if (result) {
            //console.log("result : " + result)
          }
          if (result == null || result == "") {
            Alert.alert(
              'Pesan',
              'Anda tidak terdaftar di Account CC Finpay',
              [
                { text: 'OK', style: 'cancel' },
              ],
              { cancelable: false }
            )
          } else {
            //Alert.alert(data.data.order.url);

            const uri = data.data.order.url;

            Linking.canOpenURL(uri).then(supported => {
              if (!supported) {
                //console.log('Can\'t handle url: ' + uri);
              } else {
                return Linking.openURL(uri);
              }
            }).catch(err => console.error('An error occurred', err));
          }
        })

      })
      .catch(error => {
        Alert.alert(
          'Pesan',
          'Anda tidak terdaftar di Account CC Finpay',
          [
            { text: 'OK', style: 'cancel' },
          ],
          { cancelable: false }
        )
      });
  }

  posesDonasi = () => {
    this.setState({ isModalVisible: false })
    if (this.state.nama == null || this.state.nama == "" || this.state.email == null || this.state.email == "" ||
      this.state.nominal == null || this.state.nominal == "" || this.state.payment == null || this.state.payment == "" ||
      this.state.phonenumber == null || this.state.phonenumber == "") {
      Alert.alert(
        'Pesan',
        'Nama / Email / Nominal / Nomer Telpon tidak boleh kosong',
        [
          { text: 'OK', style: 'cancel' },
        ],
        { cancelable: false }
      )
    } else {
      if (this.state.payment == 'T') {
        //T-Money
        return fetch("http://ec2-54-255-179-65.ap-southeast-1.compute.amazonaws.com:9000/api/tmoney/donation", {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
          },
          body: JSON.stringify({
            email: this.state.email,
            amount: this.state.nominal,
            no_hp: this.state.phonenumber,
            nama_lengkap: this.state.nama,
            password: this.state.password,
            pin: this.state.pin,
            tujuan: this.state.akun,
            idNews: "programkemanusiaan"
          })
        })

          .then(response => response.json())
          .then((data) => {

            AsyncStorage.setItem("hasil", data.data.status);
            AsyncStorage.getItem("hasil", (error, result) => {
              if (result) {
                //console.log("result : " + result)

              }
              if (result == null || result == "") {
                Alert.alert(
                  'Pesan',
                  'Anda tidak terdaftar di Account T-Money',
                  [

                    { text: 'OK', style: 'cancel' },
                  ],
                  { cancelable: false }
                )
              } else {
                Alert.alert(
                  'Pesan',
                  'Transfer Berhasil',
                  [
                    {
                      text: 'OK', onPress: () => {
                        this.props.navigation.navigate("Promo");
                        this.setState(this.state);
                      }
                    },
                  ],
                  { cancelable: false }
                )
              }
            })

          })
          .catch(error => {
            Alert.alert(
              'Pesan',
              'Anda tidak terdaftar di Account T-Money',
              [
                { text: 'OK', style: 'cancel' },
              ],
              { cancelable: false }
            )
          });
      }
    }
  }
}